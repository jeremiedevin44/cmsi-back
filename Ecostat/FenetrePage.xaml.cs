﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Ecostat
{
    /// <summary>
    /// Logique d'interaction pour FenetrePage.xaml
    /// </summary>
    public partial class FenetrePage : Window
    {
        public FenetrePage()
        {
            InitializeComponent();
            MainFrame.Navigate(new Uri("Pages/Enquete/ListeEnquetes.xaml", UriKind.Relative));
        }

        private void GoToAjouterAdministrateur(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Uri("Pages/Administration/AjouterUtilisateur.xaml", UriKind.Relative));
        }
        private void GoToListeAdministrateur(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Uri("Pages/Administration/ListeAdministrateur.xaml", UriKind.Relative));
        }
        private void GoToListeUtilisateur(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Uri("Pages/Utilisateur/ListeUtilisateurs.xaml", UriKind.Relative));
        }
        private void GoToAjouterEnquete(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Uri("Pages/Enquete/AjouterEnquete.xaml", UriKind.Relative));
        }
        private void GoToListeEnquete(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Uri("Pages/Enquete/ListeEnquetes.xaml", UriKind.Relative));
        }
        private void GoToAjouterSondage(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Uri("Pages/Sondage/AjouterSondage.xaml", UriKind.Relative));
        }
        private void GoToListeSondage(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Uri("Pages/Sondage/ListeSondages.xaml", UriKind.Relative));
        }
        private void GoToAjouterQuizz(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Uri("Pages/Quizz/AjouterQuizz.xaml", UriKind.Relative));
        }
        private void GoToListeQuizz(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Uri("Pages/Quizz/ListeQuizzs.xaml", UriKind.Relative));
        }
        private void GoToAjouterTheme(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Uri("Pages/Theme/AjouterTheme.xaml", UriKind.Relative));
        }
        private void GoToListeTheme(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Uri("Pages/Theme/ListeThemes.xaml", UriKind.Relative));
        }
        private void GoToAjouterRecompense(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Uri("Pages/Quizz/AjouterRecompense.xaml", UriKind.Relative));
        }
        private void GoToListeRecompense(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Uri("Pages/Quizz/ListeRecompenses.xaml", UriKind.Relative));
        }

        private void GoToMainWindow(object sender, RoutedEventArgs e)
        {
            MainWindow MainWindow = new MainWindow();
            MainWindow.Show();

            this.Close();
        }

    }
}
