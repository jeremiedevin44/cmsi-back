//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ecostat
{
    using System;
    using System.Collections.Generic;
    
    public partial class sondage_utilisateur
    {
        public int utilisateur_id { get; set; }
        public int reponse_id { get; set; }
        public bool SondageFini { get; set; }
    
        public virtual sondage sondage { get; set; }
        public virtual utilisateur utilisateur { get; set; }
    }
}
