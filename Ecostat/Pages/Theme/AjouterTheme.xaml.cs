﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat.Pages.Theme
{
    /// <summary>
    /// Logique d'interaction pour AjouterTheme.xaml
    /// </summary>
    public partial class AjouterTheme : Page
    {
        public AjouterTheme()
        {
            InitializeComponent();
        }
        
        private void ButtonCreer_Click(object sender, RoutedEventArgs e)
        {
            string nom = TextBoxNom.Text;
            string description = TextBoxDescription.Text;

            using (var db = new ecostatEntities())
            {
                int IdTheme;
                theme theme = new theme() { nom = nom, description = description };
                db.themes.Add(theme);
                db.SaveChanges();
                var req2 = from v in db.themes
                           where v.nom == nom
                           select v.id;
                IdTheme = Convert.ToInt32(req2.FirstOrDefault());


                Page PageTheme = new PageTheme(IdTheme);
                NavigationService.Navigate(PageTheme);
            }
        }
    }
}
