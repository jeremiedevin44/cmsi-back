﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat.Pages.Theme
{
    /// <summary>
    /// Logique d'interaction pour ListeThemes.xaml
    /// </summary>
    public partial class ListeThemes : Page
    {
        public ListeThemes()
        {
            using (var db = new ecostatEntities())
            {
                InitializeComponent();

                List<theme> ListeTheme = new List<theme>();

                var req = from q in db.themes
                          select new
                          {
                              q.id,
                              q.nom,
                              q.description
                          };
                foreach (var item in req)
                {
                    ListeTheme.Add(new theme() { id = item.id, nom = item.nom, description = item.description });
                }
                LTheme.ItemsSource = ListeTheme;
                LTheme.SelectedValuePath = "id";
            }
        }

        private void LTheme_MouseDoubleClick(object sender, MouseButtonEventArgs q)
        {
            if (LTheme.SelectedValue != null)
            {
                //dirige vers la page des quizzs en donnant en paramètre l'id du du quizz où on doubleclick
                int selected = Int32.Parse(LTheme.SelectedValue.ToString());

                Page PageTheme = new PageTheme(selected);
                NavigationService.Navigate(PageTheme);
            }
        }

        

        private void ButtonAjouter_click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("Pages/Theme/AjouterTheme.xaml", UriKind.Relative));
        }
    }
}
