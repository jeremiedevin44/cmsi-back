﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat.Pages.Theme
{
    /// <summary>
    /// Logique d'interaction pour PageTheme.xaml
    /// </summary>
    public partial class PageTheme : Page
    {
        int idTheme;
        public PageTheme(int parameter)
        {
            var db = new ecostatEntities();
            InitializeComponent();
            idTheme = parameter;

            var reqNomTheme = from q in db.themes
                              where q.id == idTheme
                              select q.nom;
            string NomTheme = reqNomTheme.FirstOrDefault().ToString();
            LabelNomTheme.Content = NomTheme;           
            
        }

        

        private void ButtonSupprimer_click(object sender, RoutedEventArgs e)
        {
            
            var db = new ecostatEntities();
            

            var reqDeleteTheme = (from t in db.themes
                                    where t.id == idTheme
                                    select t).FirstOrDefault();
            db.themes.Remove(reqDeleteTheme);            

            db.SaveChanges();



            Page Retour = new ListeThemes();
            NavigationService.Navigate(Retour);
        }
    }
}
