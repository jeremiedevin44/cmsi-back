﻿using Ecostat.Pages.Sondage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat.Pages.Sondages
{
    /// <summary>
    /// Logique d'interaction pour AjouterSondage.xaml
    /// </summary>
    public partial class AjouterSondage : Page
    {
        public AjouterSondage()
        {
            using (var db = new ecostatEntities())
            {
                InitializeComponent();

                List<theme> ListeTheme = new List<theme>();

                var req = from t in db.themes
                          select new
                          {
                              t.id,
                              t.nom
                          };
                foreach (var item in req)
                {
                    ListeTheme.Add(new theme() { id = item.id, nom = item.nom });
                }
                comboBox.ItemsSource = ListeTheme;
            }
        }

       

        private void ButtonCreer_Click(object sender, RoutedEventArgs e)
        {
            string titre = TextBoxTitre.Text;
            string description = TextBoxDescription.Text;
            string libelle = TextBoxQuestion.Text;

            using (var db = new ecostatEntities())
            {
                int IdSondage;
                int IdQuestion;
                List<theme> ListeThemeSelect = new List<theme>();
                ListeThemeSelect.Add(db.themes.Find((comboBox.SelectedItem as theme).id)); //Pour éviter d'instancier un nouveau thème
                sondage sondage = new sondage() { titre = titre, description = description, themes = ListeThemeSelect };
                db.sondages.Add(sondage);
                db.SaveChanges();

                var req2 = from s in db.sondages
                           where s.titre == titre
                           select s.id;
                IdSondage = Convert.ToInt32(req2.FirstOrDefault());
                question__sondage question_sondage = new question__sondage() { sondage_id = IdSondage, libelle = libelle };
                db.question__sondage.Add(question_sondage);
                db.SaveChanges();

                var req3 = from q in db.question__sondage
                           where q.libelle == libelle
                           select q.id;
                IdQuestion = Convert.ToInt32(req3.FirstOrDefault());

                Page AjouterReponsesSondage = new AjouterReponsesSondage(IdQuestion);
                NavigationService.Navigate(AjouterReponsesSondage);
            }
        }
    }
}
