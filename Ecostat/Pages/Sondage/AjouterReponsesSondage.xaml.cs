﻿using Ecostat.Pages.Sondages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat.Pages.Sondage
{
    /// <summary>
    /// Logique d'interaction pour AjouterReponsesSondage.xaml
    /// </summary>
    public partial class AjouterReponsesSondage : Page
    {
        private int idQuestion;
        public AjouterReponsesSondage(int parameter)
        {
            var db = new ecostatEntities();
            InitializeComponent();
            idQuestion = parameter;

            var reqNomQuestion = from q in db.question__sondage
                                 where q.id == idQuestion
                                 select q.libelle;
            string NomQuestion = reqNomQuestion.FirstOrDefault().ToString();
            LabelNomQuestion.Content = NomQuestion;



            List<reponse__sondage> ListeReponses = db.reponse__sondage.Where(x => x.question_id == idQuestion).ToList();

            LReponses.ItemsSource = ListeReponses;
            LReponses.SelectedValuePath = "id";
        }

        private void Refresh()
        {
            var db = new ecostatEntities();
            List<reponse__sondage> ListeReponses = db.reponse__sondage.Where(x => x.question_id == idQuestion).ToList();

            LReponses.ItemsSource = ListeReponses;
        }

        

        private void ButtonReponse_Click(object sender, RoutedEventArgs e)
        {
            var db = new ecostatEntities();
            reponse__sondage reponse_sondage = new reponse__sondage { question_id = idQuestion, contenu = TextBoxReponse.Text };
            db.reponse__sondage.Add(reponse_sondage);
            db.SaveChanges();
            TextBoxReponse.Clear();
            Refresh();
        }

        private void ButtonSupprimer_click(object sender, RoutedEventArgs e)
        {
            int idSondage;
            var db = new ecostatEntities();

            var reqIdSondage = (from que in db.question__sondage
                                 where que.id == idQuestion
                                 select que.sondage_id).FirstOrDefault();
            idSondage = Convert.ToInt32(reqIdSondage);

            var reqDeleteQuestion = (from qu in db.question__sondage
                                     where qu.id == idQuestion
                                     select qu).FirstOrDefault();
            db.question__sondage.Remove(reqDeleteQuestion);

            var reqDeleteSondage = (from so in db.sondages
                                   where so.id == idSondage
                                   select so).FirstOrDefault();
            db.sondages.Remove(reqDeleteSondage);

            var reqDeleteReponse = from re in db.reponse__sondage
                                   where re.question_id == idQuestion
                                   select re;

            foreach (var item in reqDeleteReponse)
            {
                db.reponse__sondage.Remove(item);
            }

            db.SaveChanges();



            Page Retour = new ListeSondages();
            NavigationService.Navigate(Retour);
        }
    }
}
