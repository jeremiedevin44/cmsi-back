﻿using Ecostat.Pages.Sondage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat.Pages.Sondages
{
    /// <summary>
    /// Logique d'interaction pour ListeSondages.xaml
    /// </summary>
    public partial class ListeSondages : Page
    {
        public ListeSondages()
        {
            using (var db = new ecostatEntities())
            {
                InitializeComponent();

                List<sondage> ListeSondage = new List<sondage>();

                var req = from q in db.sondages
                          select new
                          {
                              q.id,
                              q.titre,
                              q.description
                          };
                foreach (var item in req)
                {
                    ListeSondage.Add(new sondage() { id = item.id, titre = item.titre, description = item.description });
                }
                LSondage.ItemsSource = ListeSondage;
                LSondage.SelectedValuePath = "id";
            }
        }

        private void LSondage_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (LSondage.SelectedValue != null)
            {
                //dirige vers la page des sondages en donnant en paramètre l'id du du sondage où on doubleclick
                int selected = Int32.Parse(LSondage.SelectedValue.ToString());

                Page PageSondage = new AjouterReponsesSondage(selected);
                NavigationService.Navigate(PageSondage);
            }
        }

        

        private void ButtonAjouter_click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("Pages/Sondage/AjouterSondage.xaml", UriKind.Relative));
        }
    }
}
