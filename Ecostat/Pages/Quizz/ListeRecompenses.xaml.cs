﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat.Pages.Quizz
{
    /// <summary>
    /// Logique d'interaction pour ListeRecompenses.xaml
    /// </summary>
    public partial class ListeRecompenses : Page
    {
        public ListeRecompenses()
        {
            using (var db = new ecostatEntities())
            {
                InitializeComponent();

                List<recompense> ListeRecompense = new List<recompense>();

                var req = from r in db.recompenses
                          select new
                          {
                              r.id,
                              r.Nom,
                              r.Entreprise
                          };
                foreach (var item in req)
                {
                    ListeRecompense.Add(new recompense() { id = item.id, Nom = item.Nom, Entreprise = item.Entreprise });
                }
                LRecompense.ItemsSource = ListeRecompense;
                LRecompense.SelectedValuePath = "id";
            }
        }

        private void LRecompense_MouseDoubleClick(object sender, MouseButtonEventArgs q)
        {
            if (LRecompense.SelectedValue != null)
            {
                //dirige vers la page des quizzs en donnant en paramètre l'id du du quizz où on doubleclick
                int selected = Int32.Parse(LRecompense.SelectedValue.ToString());

                Page PageRecompense = new PageRecompense(selected);
                NavigationService.Navigate(PageRecompense);
            }
        }

        

        private void ButtonAjouter_click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("Pages/Quizz/AjouterRecompense.xaml", UriKind.Relative));
        }
    }
}
