﻿using Ecostat.Pages.Quizzs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat.Pages.Quizz
{
    /// <summary>
    /// Logique d'interaction pour AjouterQuestionsQuizz.xaml
    /// </summary>
    public partial class AjouterQuestionsQuizz : Page
    {
        private int idQuizz;
        public AjouterQuestionsQuizz(int parameter)
        {
            var db = new ecostatEntities();
            InitializeComponent();
            idQuizz = parameter;

            var reqNomQuizz = from q in db.quizzs
                              where q.id == idQuizz
                              select q.nom;
            string NomQuizz = reqNomQuizz.FirstOrDefault().ToString();
            LabelNomQuizz.Content = NomQuizz;



            List<question__quizz> ListeQuestions = db.question__quizz.Where(x => x.quizz_id == idQuizz).ToList();

            LQuestions.ItemsSource = ListeQuestions;
            LQuestions.SelectedValuePath = "id";
        }

        private void Refresh()
        {
            var db = new ecostatEntities();
            List<question__quizz> ListeQuestion = db.question__quizz.Where(x => x.quizz_id == idQuizz).ToList();

            LQuestions.ItemsSource = ListeQuestion;
        }
        
        private void ButtonAddQuestion_Click(object sender, RoutedEventArgs e)
        {
            var db = new ecostatEntities();
            question__quizz question_quizz = new question__quizz { quizz_id = idQuizz, libelle = TextBoxLibelleQuestion.Text };
            db.question__quizz.Add(question_quizz);
            db.SaveChanges();
            TextBoxLibelleQuestion.Clear();
            Refresh();
        }

        private void LQuestions_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (LQuestions.SelectedValue != null)
            {
                int selected = Int32.Parse(LQuestions.SelectedValue.ToString());

                Page AjouterReponses = new AjouterReponsesQuestionQuizz(selected);
                NavigationService.Navigate(AjouterReponses);
            }
        }

        private void ButtonSupprimer_click(object sender, RoutedEventArgs e)
        {
            var db = new ecostatEntities();

            
            var reqDeleteQuizz = (from q in db.quizzs
                                     where q.id == idQuizz
                                     select q).FirstOrDefault();

            db.quizzs.Remove(reqDeleteQuizz);

            var reqIdQuestion = from qu in db.question__quizz
                                where qu.quizz_id == idQuizz
                                select qu.id;
            var reqDeleteQuestion = from qu in db.question__quizz
                                    where qu.quizz_id == idQuizz
                                    select qu;
            
            foreach (var item in reqDeleteQuestion)
            {
                db.question__quizz.Remove(item);

                foreach (var item2 in reqIdQuestion)
                {
                    var reqDeleteReponse = from re in db.reponse__quizz
                                           where re.question_id == item2
                                           select re;
                    foreach (var item3 in reqDeleteReponse)
                    {
                        db.reponse__quizz.Remove(item3);
                    }
                }
            }
            var reqDeleteRecompense = from qu in db.recompenses
                                      where qu.quizz_id == idQuizz
                                      select qu;
            foreach (var item in reqDeleteRecompense)
            {
                db.recompenses.Remove(item);
            }

            db.SaveChanges();



            Page Retour = new ListeQuizzs();
            NavigationService.Navigate(Retour);
        }
    }
}
