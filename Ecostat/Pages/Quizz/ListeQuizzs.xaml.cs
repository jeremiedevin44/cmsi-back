﻿using Ecostat.Pages.Quizz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat.Pages.Quizzs
{
    /// <summary>
    /// Logique d'interaction pour ListeQuizzs.xaml
    /// </summary>
    public partial class ListeQuizzs : Page
    {
        public ListeQuizzs()
        {
            using (var db = new ecostatEntities())
            {
                InitializeComponent();

                List<quizz> ListeQuizz = new List<quizz>();

                var req = from q in db.quizzs
                          select new
                          {
                              q.id,
                              q.nom,
                              q.description
                          };
                foreach (var item in req)
                {
                    ListeQuizz.Add(new quizz() { id = item.id, nom = item.nom, description = item.description });
                }
                LQuizz.ItemsSource = ListeQuizz;
                LQuizz.SelectedValuePath = "id";
            }
        }

        private void LQuizz_MouseDoubleClick(object sender, MouseButtonEventArgs q)
        {
            if (LQuizz.SelectedValue != null)
            {
                //dirige vers la page des quizzs en donnant en paramètre l'id du du quizz où on doubleclick
                int selected = Int32.Parse(LQuizz.SelectedValue.ToString());

                Page AjouterQuestions = new AjouterQuestionsQuizz(selected);
                NavigationService.Navigate(AjouterQuestions);
            }
        }

        

        private void ButtonAjouter_click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("Pages/Quizz/AjouterQuizz.xaml", UriKind.Relative));
        }
    }
}
