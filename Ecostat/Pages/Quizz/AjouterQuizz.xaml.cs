﻿using Ecostat.Pages.Quizz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat.Pages.Quizzs
{
    /// <summary>
    /// Logique d'interaction pour AjouterQuizz.xaml
    /// </summary>
    public partial class AjouterQuizz : Page
    {
        public AjouterQuizz()
        {
            InitializeComponent();
        }
        

        private void ButtonCreer_Click(object sender, RoutedEventArgs e)
        {
            string nom = TextBoxNom.Text;
            string description = TextBoxDescription.Text;

            using (var db = new ecostatEntities())
            {
                int IdQuizz;
                quizz quizz = new quizz() { nom = nom, description = description };
                db.quizzs.Add(quizz);
                db.SaveChanges();
                var req2 = from v in db.quizzs
                           where v.nom == nom
                           select v.id;
                IdQuizz = Convert.ToInt32(req2.FirstOrDefault());


                Page AjouterQuestions = new AjouterQuestionsQuizz(IdQuizz);
                NavigationService.Navigate(AjouterQuestions);
             }
        }
    }
}
