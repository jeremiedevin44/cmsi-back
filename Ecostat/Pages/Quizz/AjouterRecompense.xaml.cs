﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat.Pages.Quizz
{
    /// <summary>
    /// Logique d'interaction pour AjouterRecompense.xaml
    /// </summary>
    public partial class AjouterRecompense : Page
    {
        public AjouterRecompense()
        {
            using (var db = new ecostatEntities())
            {
                InitializeComponent();

                List<quizz> ListeQuizz = new List<quizz>();

                var req = from q in db.quizzs
                          select new
                          {
                              q.id,
                              q.nom
                          };
                foreach (var item in req)
                {
                    ListeQuizz.Add(new quizz() { id = item.id, nom = item.nom });
                }
                comboBox.ItemsSource = ListeQuizz;
            }
        }
        private void ButtonCreer_Click(object sender, RoutedEventArgs e)
        {
            string nom = TextBoxNom.Text;
            string entreprise = TextBoxEntreprise.Text;
            
            using (var db = new ecostatEntities())
            {
                int idQuizz = (comboBox.SelectedItem as quizz).id;
                recompense recompense = new recompense() { Nom = nom, Entreprise = entreprise, quizz_id = idQuizz };

                db.recompenses.Add(recompense);

                db.SaveChanges();


                Page ListeRecompense = new ListeRecompenses();
                NavigationService.Navigate(ListeRecompense);
            }
        }
    }
}
