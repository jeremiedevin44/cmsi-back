﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat.Pages.Quizz
{
    /// <summary>
    /// Logique d'interaction pour PageRecompense.xaml
    /// </summary>
    public partial class PageRecompense : Page
    {
        int idRecompense;
        public PageRecompense(int parameter)
        {
            var db = new ecostatEntities();
            InitializeComponent();
            idRecompense = parameter;

            var reqNomRecompense = from q in db.recompenses
                              where q.id == idRecompense
                              select q.Nom;
            string NomRecompense = reqNomRecompense.FirstOrDefault().ToString();
            LabelNomRecompense.Content = NomRecompense;

        }

        

        private void ButtonSupprimer_click(object sender, RoutedEventArgs e)
        {

            var db = new ecostatEntities();


            var reqDeleteRecompense = (from t in db.recompenses
                                  where t.id == idRecompense
                                  select t).FirstOrDefault();
            db.recompenses.Remove(reqDeleteRecompense);

            db.SaveChanges();



            Page Retour = new ListeRecompenses();
            NavigationService.Navigate(Retour);
        }
    }
}
