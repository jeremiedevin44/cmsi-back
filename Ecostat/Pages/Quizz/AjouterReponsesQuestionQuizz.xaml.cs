﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat.Pages.Quizz
{
    /// <summary>
    /// Logique d'interaction pour AjouterReponsesQuestionQuizz.xaml
    /// </summary>
    public partial class AjouterReponsesQuestionQuizz : Page
    {
        private int idQuestion;
        public AjouterReponsesQuestionQuizz(int parameter)
        {
            var db = new ecostatEntities();
            InitializeComponent();
            idQuestion = parameter;

            var reqNomQuestion = from q in db.question__quizz
                              where q.id == idQuestion
                              select q.libelle;
            string NomQuestion = reqNomQuestion.FirstOrDefault().ToString();
            LabelNomQuestion.Content = NomQuestion;



            List<reponse__quizz> ListeReponses = db.reponse__quizz.Where(x => x.question_id == idQuestion).ToList();

            LReponses.ItemsSource = ListeReponses;
            LReponses.SelectedValuePath = "id";
        }

        private void Refresh()
        {
            var db = new ecostatEntities();
            List<reponse__quizz> ListeReponses = db.reponse__quizz.Where(x => x.question_id == idQuestion).ToList();

            LReponses.ItemsSource = ListeReponses;
        }
        
        private void ButtonAddReponse_Click(object sender, RoutedEventArgs e)
        {
            var db = new ecostatEntities();
            reponse__quizz reponse_quizz = new reponse__quizz { question_id = idQuestion, contenu = TextBoxLibelleReponse.Text };
            db.reponse__quizz.Add(reponse_quizz);
            db.SaveChanges();
            TextBoxLibelleReponse.Clear();
            Refresh();
        }

        private void ButtonSupprimer_click(object sender, RoutedEventArgs e)
        {
            int idQuizz;
            var db = new ecostatEntities();

            var reqIdQuizz = (from q in db.question__quizz
                                 where q.id == idQuestion
                                 select q.quizz_id).FirstOrDefault();
            idQuizz = Convert.ToInt32(reqIdQuizz);


            var reqDeleteQuestion = (from qu in db.question__quizz
                                     where qu.id == idQuestion
                                     select qu).FirstOrDefault();

            db.question__quizz.Remove(reqDeleteQuestion);

            var reqDeleteReponse = from r in db.reponse__quizz
                                   where r.question_id == idQuestion
                                   select r;

            foreach (var item in reqDeleteReponse)
            {
                db.reponse__quizz.Remove(item);
            }
            
            
            db.SaveChanges();



            Page Retour = new AjouterQuestionsQuizz(idQuizz);
            NavigationService.Navigate(Retour);
        }
    }
}
