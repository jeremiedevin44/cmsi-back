﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat.Pages.Utilisateurs
{
    /// <summary>
    /// Logique d'interaction pour PageUtilisateur.xaml
    /// </summary>
    public partial class PageUtilisateur : Page
    {

        int idUtilisateur;

        public PageUtilisateur(int parameter)
        {
            var db = new ecostatEntities();
            InitializeComponent();


            int idutilisateur = parameter;

            idUtilisateur = parameter;


            var reqNomUtilisateur = from u in db.utilisateurs
                                    where u.id == idUtilisateur
                                    select u.nom;
            foreach (var item in reqNomUtilisateur)
            {
                //Met le nom de l'utilisateur à la place du nom du label de l'utilisateur
                labelNomUtilisateur.Content = item;
            }

            var reqPrenomUtilisateur = from u in db.utilisateurs
                                       where u.id == idUtilisateur
                                       select u.prenom;
            foreach (var item2 in reqPrenomUtilisateur)
            {
                //Met le nom de l'utilisateur à la place du nom du label de l'utilisateur
                labelPrenomUtilisateur.Content = item2;
            }

            List<question__sondage> ListeSondage = new List<question__sondage>();

            var req = from s in db.sondages
                      join qs in db.question__sondage
                        on s.id equals qs.sondage_id
                      join rs in db.reponse__sondage
                        on qs.id equals rs.question_id
                      join urs in db.utilisateur_reponse_sondage
                        on rs.id equals urs.reponse_id
                      where urs.utilisateur_id == idUtilisateur
                      select new
                      {
                          qs.libelle
                      };
            foreach (var item in req)
            {
                ListeSondage.Add(new question__sondage() { libelle = item.libelle });
            }
            LSondages.ItemsSource = ListeSondage;


            List<enquete> ListeEnquete = new List<enquete>();

            var req2 = from e in db.enquetes
                      join s in db.sequences
                        on e.id equals s.enquete_id
                      join qe in db.question__enquete
                        on s.id equals qe.sequence_id
                      join re in db.reponse__enquete
                        on qe.id equals re.question_id
                      join ure in db.utilisateur_reponse_enquete
                        on re.id equals ure.reponse_id
                      where ure.utilisateur_id == idUtilisateur
                      group e.titre by e.titre into g
                      select new
                      {
                          g.Key
                      };
            foreach (var item in req2)
            {
                ListeEnquete.Add(new enquete() { titre = item.Key });
            }
            LEnquetes.ItemsSource = ListeEnquete;


            List<quizz> ListeQuizz = new List<quizz>();

            var req3 = from q in db.quizzs
                       join qq in db.question__quizz
                         on q.id equals qq.quizz_id
                       join rq in db.reponse__quizz
                         on qq.id equals rq.question_id
                       join urq in db.utilisateur_reponse_quizz
                         on rq.id equals urq.reponse_id
                       where urq.utilisateur_id == idUtilisateur
                       group q.nom by q.nom into g
                       select new
                       {
                           g.Key
                       };
            foreach (var item in req3)
            {
                ListeQuizz.Add(new quizz() { nom = item.Key });
            }
            LQuizzs.ItemsSource = ListeQuizz;
        }

        
    }
}
