﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat.Pages.Utilisateurs
{
    /// <summary>
    /// Logique d'interaction pour ListeUtilisateurs.xaml
    /// </summary>
    public partial class ListeUtilisateurs : Page
    {
        public ListeUtilisateurs()
        {
            using (var db = new ecostatEntities())
            {
                InitializeComponent();

                List<utilisateur> ListeUtilisateur = new List<utilisateur>();

                var req = from u in db.utilisateurs
                          select new
                          {
                              u.id,
                              u.nom,
                              u.prenom
                          };
                foreach (var item in req)
                {
                    ListeUtilisateur.Add(new utilisateur() { id = item.id, nom = item.nom, prenom = item.prenom });
                }
                LUtilisateur.ItemsSource = ListeUtilisateur;
                LUtilisateur.SelectedValuePath = "id";
            }
        }

        

        private void LUtilisateur_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (LUtilisateur.SelectedValue != null)
            {
                int selected = Int32.Parse(LUtilisateur.SelectedValue.ToString());

                Page PageUtilisateur = new PageUtilisateur(selected);
                NavigationService.Navigate(PageUtilisateur);
            }
        }
    }
}
