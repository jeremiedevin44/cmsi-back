﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat.Pages.Administration
{
    /// <summary>
    /// Logique d'interaction pour ListeAdministrateur.xaml
    /// </summary>
    public partial class ListeAdministrateur : Page
    {
        public ListeAdministrateur()
        {
            using (var db = new ecostatEntities())
            {
                InitializeComponent();

                List<user> ListeAdministrateur = new List<user>();

                var req = from u in db.users
                          select new
                          {
                              u.id,
                              u.login,
                          };
                foreach (var item in req)
                {
                    ListeAdministrateur.Add(new user() { id = item.id, login = item.login });
                }
                LAdministrateur.ItemsSource = ListeAdministrateur;
                LAdministrateur.SelectedValuePath = "id";
            }
        }
        
        private void LAdministrateur_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (LAdministrateur.SelectedValue != null)
            {
                int selected = Int32.Parse(LAdministrateur.SelectedValue.ToString());

                Page PageAdministrateur = new PageAdministrateur(selected);
                NavigationService.Navigate(PageAdministrateur);
            }
        }

        private void ButtonAjouter_click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("Pages/Administration/AjouterUtilisateur.xaml", UriKind.Relative));
        }
    }
}
