﻿using Ecostat.Pages.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat.Pages.Utilisateurs
{
    /// <summary>
    /// Logique d'interaction pour AjouterUtilisateur.xaml
    /// </summary>
    public partial class AjouterUtilisateur : Page
    {
        public AjouterUtilisateur()
        {
            InitializeComponent();
        }

        

        private void ButtonCreer_Click(object sender, RoutedEventArgs e)
        {
            
                if (PasswordBox1.Password.Length == 0)
                {
                    LabelErreur.Text = "Entrez un mot de passe";
                    PasswordBox1.Focus();
                }
                else if (PasswordBox2.Password.Length == 0)
                {
                    LabelErreur.Text = "Entrez un mot de passe conforme";
                    PasswordBox2.Focus();
                }
                else if (PasswordBox1.Password != PasswordBox2.Password)
                {
                    LabelErreur.Text = "Les deux mots de passe doivent être les mêmes!";
                    PasswordBox2.Focus();
                }
                else
                {

                    using (var db = new ecostatEntities())
                    {
                        string login = TextBoxLogin.Text;
                        string mdp = PasswordBox1.Password;

                        int IdUser;
                        user user = new user() { login = login, mdp = mdp };
                        db.users.Add(user);
                        db.SaveChanges();
                        var req2 = from u in db.users
                                   where u.login == login
                                   select u.id;
                        IdUser = Convert.ToInt32(req2.FirstOrDefault());

                        Page ListeAdministrateur = new ListeAdministrateur();
                        NavigationService.Navigate(ListeAdministrateur);
                    }
                }
            }

        }
    }
