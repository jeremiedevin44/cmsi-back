﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat.Pages.Administration
{
    /// <summary>
    /// Logique d'interaction pour PageAdministrateur.xaml
    /// </summary>
    public partial class PageAdministrateur : Page
    {
        int idAdmin;
        public PageAdministrateur(int parameter)
        {
            var db = new ecostatEntities();
            InitializeComponent();
            idAdmin = parameter;

            var reqNomAdmin = from u in db.users
                              where u.id == idAdmin
                              select u.login;
            string NomAdmin = reqNomAdmin.FirstOrDefault().ToString();
            LabelNomAdmin.Content = NomAdmin;

        }

        

        private void ButtonSupprimer_click(object sender, RoutedEventArgs e)
        {

            var db = new ecostatEntities();


            var reqDeleteAdmin = (from u in db.users
                                  where u.id == idAdmin
                                  select u).FirstOrDefault();
            db.users.Remove(reqDeleteAdmin);

            db.SaveChanges();



            Page Retour = new ListeAdministrateur();
            NavigationService.Navigate(Retour);
        }
    }
}
