﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat.Pages.Enquete.Sequence
{
    /// <summary>
    /// Logique d'interaction pour AjouterSequences.xaml
    /// </summary>
    public partial class AjouterSequences : Page
    {
        private int idEnquete;
        public AjouterSequences(int parameter)
        {
            var db = new ecostatEntities();
            InitializeComponent();
            idEnquete = parameter;

            var reqNomEnquete = from e in db.enquetes
                                where e.id == idEnquete
                                select e.titre;
            string NomEnquete = reqNomEnquete.FirstOrDefault().ToString();
            LabelNomEnquete.Content = NomEnquete;



            List<sequence> ListeSequence = db.sequences.Include(x => x.etat).Where(x => x.enquete_id == idEnquete).ToList();

            LSequences.ItemsSource = ListeSequence;
            LSequences.SelectedValuePath = "id";
        }

        private void Refresh()
        {
            var db = new ecostatEntities();
            List<sequence> ListeSequence = db.sequences.Include(x => x.etat).Where(x => x.enquete_id == idEnquete).ToList();

            LSequences.ItemsSource = ListeSequence;
        }

        

        private void ButtonSequence_Click(object sender, RoutedEventArgs e)
        {
            var db = new ecostatEntities();

            sequence Sequence = new sequence { enquete_id = idEnquete, etat_id = 1, dateCreation = DateDebutSequence.SelectedDate, nom = TextBoxSequence.Text };
            db.sequences.Add(Sequence);
            db.SaveChanges();
            TextBoxSequence.Clear();
            DateDebutSequence.ClearValue(DatePicker.SelectedDateProperty);
            Refresh();
        }

        private void LSequences_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (LSequences.SelectedValue != null)
            {
                int selected = Int32.Parse(LSequences.SelectedValue.ToString());

                Page AjouterQuestions = new AjouterQuestionsSequence(selected);
                NavigationService.Navigate(AjouterQuestions);
            }
        }

        private void ButtonListeEnquetes_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("Pages/Enquete/ListeEnquetes.xaml", UriKind.Relative));
        }

        private void ButtonSupprimer_click(object sender, RoutedEventArgs e)
        {
            var db = new ecostatEntities();
            // On supprime l'enquête dans la base de données
            var reqDeleteEnquete = (from eq in db.enquetes
                             where eq.id == idEnquete
                             select eq).FirstOrDefault();
            db.enquetes.Remove(reqDeleteEnquete);

            //On récupère l'id de toutes les séquences liées à l'enquête supprimée
            var reqIdSequence = from s in db.sequences
                                where s.enquete_id == idEnquete
                                select s.id;
            // On supprime toutes les séquences liées a l'enquête supprimée
            var reqDeleteSequence = from seq in db.sequences
                                    where seq.enquete_id == idEnquete
                                    select seq;
            foreach (var item in reqDeleteSequence)
            {
                db.sequences.Remove(item);
            }

            //pour chaque séquence supprimée, on supprimes les questions associées
            foreach (var item in reqIdSequence)
            {
                var reqIdQuestion = from q in db.question__enquete
                                     where q.sequence_id == item
                                     select q.id;
                var reqDeleteQuestion = from qu in db.question__enquete
                                        where qu.sequence_id == item
                                        select qu;
                foreach (var item2 in reqDeleteQuestion)
                {
                    db.question__enquete.Remove(item2);
                }

                //Pour chaque question supprimée, on supprime les réponses possibles associées
                foreach (var item2 in reqIdQuestion)
                {
                    var reqDeleteReponse = from re in db.reponse__enquete
                                           where re.question_id == item2
                                           select re;
                    foreach (var item3 in reqDeleteReponse)
                    {
                        db.reponse__enquete.Remove(item3);
                    }
                }
            }

            

            db.SaveChanges();
            this.NavigationService.Navigate(new Uri("Pages/Enquete/ListeEnquetes.xaml", UriKind.Relative));
        }
    }
}
