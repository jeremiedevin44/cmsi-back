﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat.Pages.Enquete.Sequence
{
    /// <summary>
    /// Logique d'interaction pour AjouterReponsesQuestionSequence.xaml
    /// </summary>
    public partial class AjouterReponsesQuestionSequence : Page
    {
        private int idQuestion;
        public AjouterReponsesQuestionSequence(int parameter)
        {
            var db = new ecostatEntities();
            InitializeComponent();
            idQuestion = parameter;

            var reqNomQuestion = from q in db.question__enquete
                                 where q.id == idQuestion
                                 select q.intitule;
            string NomQuestion = reqNomQuestion.FirstOrDefault().ToString();
            LabelNomQuestion.Content = NomQuestion;



            List<reponse__enquete> ListeReponses = db.reponse__enquete.Where(x => x.question_id == idQuestion).ToList();

            LReponses.ItemsSource = ListeReponses;
            LReponses.SelectedValuePath = "id";
        }

        private void Refresh()
        {
            var db = new ecostatEntities();
            List<reponse__enquete> ListeReponses = db.reponse__enquete.Where(x => x.question_id == idQuestion).ToList();

            LReponses.ItemsSource = ListeReponses;
        }

        

        private void ButtonReponse_Click(object sender, RoutedEventArgs e)
        {
            var db = new ecostatEntities();
            reponse__enquete reponse_enquete = new reponse__enquete { question_id = idQuestion, contenu = TextBoxReponse.Text };
            db.reponse__enquete.Add(reponse_enquete);
            db.SaveChanges();
            TextBoxReponse.Clear();
            Refresh();
        }

        private void ButtonSupprimer_click(object sender, RoutedEventArgs e)
        {
            int idSequence;
            var db = new ecostatEntities();

            var reqIdSequence = (from que in db.question__enquete
                                 where que.id == idQuestion
                                 select que.sequence_id).FirstOrDefault();
            idSequence = Convert.ToInt32(reqIdSequence);

            var reqDeleteQuestion = (from qu in db.question__enquete
                                    where qu.id == idQuestion
                                    select qu).FirstOrDefault();

            db.question__enquete.Remove(reqDeleteQuestion);

            var reqDeleteReponse = from re in db.reponse__enquete
                                   where re.question_id == idQuestion
                                   select re;

            foreach (var item in reqDeleteReponse)
            {
                db.reponse__enquete.Remove(item);
            }

            db.SaveChanges();

            
            
            Page Retour = new AjouterQuestionsSequence(idSequence);
            NavigationService.Navigate(Retour);
        }

        private void ButtonRetour_Click(object sender, RoutedEventArgs e)
        {
            int idSequence;
            var db = new ecostatEntities();

            var reqIdSequence = (from que in db.question__enquete
                                where que.id == idQuestion
                                select que.sequence_id).FirstOrDefault();
            idSequence = Convert.ToInt32(reqIdSequence);

            Page Retour = new AjouterQuestionsSequence(idSequence);
            NavigationService.Navigate(Retour);
        }
    }
}
