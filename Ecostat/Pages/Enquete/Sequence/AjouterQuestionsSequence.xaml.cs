﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat.Pages.Enquete.Sequence
{
    /// <summary>
    /// Logique d'interaction pour AjouterQuestionsSequence.xaml
    /// </summary>
    public partial class AjouterQuestionsSequence : Page
    {
        private int idSequence;
        public AjouterQuestionsSequence(int parameter)
        {
            var db = new ecostatEntities();
            InitializeComponent();
            idSequence = parameter;

            var reqNomSequence = from s in db.sequences
                                where s.id == idSequence
                                select s.nom;
            string NomSequence = reqNomSequence.FirstOrDefault().ToString();
            LabelNomSequence.Content = NomSequence;



            List<question__enquete> ListeQuestions = db.question__enquete.Where(x => x.sequence_id == idSequence).ToList();

            LQuestions.ItemsSource = ListeQuestions;
            LQuestions.SelectedValuePath = "id";
        }

        private void Refresh()
        {
            var db = new ecostatEntities();
            List<question__enquete> ListeQuestions = db.question__enquete.Where(x => x.sequence_id == idSequence).ToList();

            LQuestions.ItemsSource = ListeQuestions;
        }

        

        private void LQuestions_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (LQuestions.SelectedValue != null)
            {
                int selected = Int32.Parse(LQuestions.SelectedValue.ToString());

                Page AjouterReponses = new AjouterReponsesQuestionSequence(selected);
                NavigationService.Navigate(AjouterReponses);
            }
        }
        int ordre = 0;
        private void ButtonQuestion_Click(object sender, RoutedEventArgs e)
        {
            var db = new ecostatEntities();
            ordre++;
            question__enquete question_enquete = new question__enquete { sequence_id = idSequence, intitule = TextBoxQuestion.Text, ordre = ordre };
            db.question__enquete.Add(question_enquete);
            db.SaveChanges();
            TextBoxQuestion.Clear();
            Refresh();
        }

        private void ButtonSupprimer_click(object sender, RoutedEventArgs e)
        {
            
            int idEnquete;
            var db = new ecostatEntities();

            var reqIdEnquete = (from seq in db.sequences
                                where seq.id == idSequence
                                select seq.enquete_id).FirstOrDefault();
            idEnquete = Convert.ToInt32(reqIdEnquete);

            var reqDeleteSequence = (from seq in db.sequences
                                    where seq.id == idSequence
                                    select seq).FirstOrDefault();
            
            db.sequences.Remove(reqDeleteSequence);
            
            var reqIdQuestion = from q in db.question__enquete
                                where q.sequence_id == idSequence
                                select q.id;
            var reqDeleteQuestion = from qu in db.question__enquete
                                    where qu.sequence_id == idSequence
                                    select qu;
            foreach (var item in reqDeleteQuestion)
            {
                db.question__enquete.Remove(item);

                foreach (var item2 in reqIdQuestion)
                {
                    var reqDeleteReponse = from re in db.reponse__enquete
                                           where re.question_id == item2
                                           select re;
                    foreach (var item3 in reqDeleteReponse)
                    {
                        db.reponse__enquete.Remove(item3);
                    }
                }
            }

            db.SaveChanges();

            

            Page Retour = new AjouterSequences(idEnquete);
            NavigationService.Navigate(Retour);
        }

        private void ButtonRetour_Click(object sender, RoutedEventArgs e)
        {
            int idEnquete;
            var db = new ecostatEntities();

            var reqIdEnquete = (from seq in db.sequences
                                where seq.id == idSequence
                                select seq.enquete_id).FirstOrDefault();
            idEnquete = Convert.ToInt32(reqIdEnquete);

            Page Retour = new AjouterSequences(idEnquete);
            NavigationService.Navigate(Retour);
        }
    }
}
