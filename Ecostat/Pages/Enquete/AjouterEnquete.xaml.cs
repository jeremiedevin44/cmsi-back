﻿using Ecostat.Pages.Enquete.Sequence;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat.Pages.Enquete
{
    /// <summary>
    /// Logique d'interaction pour AjouterEnquete.xaml
    /// </summary>
    public partial class AjouterEnquete : Page
    {
        public AjouterEnquete()
        {
            using (var db = new ecostatEntities())
            {
                InitializeComponent();

                List<theme> ListeTheme = new List<theme>();

                var req = from t in db.themes
                          select new
                          {
                              t.id,
                              t.nom
                          };
                foreach (var item in req)
                {
                    ListeTheme.Add(new theme() { id = item.id, nom = item.nom });
                }
                comboBox.ItemsSource = ListeTheme;
            }
            
        }

        
        private void ButtonCreer_Click(object sender, RoutedEventArgs e)
        {
            string titre = TextBoxTitre.Text;
            string description = TextBoxDescription.Text;

            using (var db = new ecostatEntities())
            {
                // On crée un nouvel objet avec les valeurs des Textbox, puis on ajoute ce nouvel objet dans la base de données
                int IdEnquete;
                List<theme> ListeThemeSelect = new List<theme>();
                ListeThemeSelect.Add(db.themes.Find((comboBox.SelectedItem as theme).id)); //Pour éviter d'instancier un nouveau thème, on donne à ListeThemeSelect le theme de l'item selectionné
                enquete enquete = new enquete() { titre = titre, description = description, themes = ListeThemeSelect };
                db.enquetes.Add(enquete);
                
                db.SaveChanges();
                
                // On réqupère l'id de l'enquête que l'on vient de rentrer
                var req2 = from v in db.enquetes
                           where v.titre == titre
                           select v.id;

                
                IdEnquete = Convert.ToInt32(req2.FirstOrDefault());

                // On dirige vers la page d'ajout de Séquences correspondant a l'enquête que l'on vient de renter
                Page AjouterSequence = new AjouterSequences(IdEnquete);
                NavigationService.Navigate(AjouterSequence);
            }
        }
    }
}
