﻿using Ecostat.Pages.Enquete.Sequence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat.Pages.Enquete
{
    /// <summary>
    /// Logique d'interaction pour ListeEnquetes.xaml
    /// </summary>
    public partial class ListeEnquetes : Page
    {
        public ListeEnquetes()
        {
            using (var db = new ecostatEntities())
            {
                InitializeComponent();

                // On crée la liste des enquêtes qui seront affichées dans le tableau
                List<enquete> ListeEnquete = new List<enquete>();

                //On récupère l'id, le titre et la description de chaque enquête présente dans la BDD
                var req = from e in db.enquetes
                          select new
                          {
                              e.id,
                              e.titre,
                              e.description
                          };
                //Pour chaque résultat de la requête, on crée un nouvel objet enquête contenant les données récupérées de la requête qu'on ajoute à la liste créée auparavant
                foreach (var item in req)
                {
                    ListeEnquete.Add(new enquete() { id = item.id, titre = item.titre, description = item.description });
                }
                // On indique enfin au tableau de prendre comme source de données la liste contenant toutes les enquêtes
                LEnquete.ItemsSource = ListeEnquete;

                //Cette ligne définit que lorsqu'on selectionne une ligne du tableau, elle soit définie par son id (pour permettre la navigation)
                LEnquete.SelectedValuePath = "id";
            }
        }

        private void LEnquete_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //dirige vers la page des séquences en donnant en paramètre l'id du de l'enquête où on doubleclick
            if(LEnquete.SelectedValue != null) { 
            int selected = Int32.Parse(LEnquete.SelectedValue.ToString());

            Page AjouterSequence = new AjouterSequences(selected);
            NavigationService.Navigate(AjouterSequence);

            }
        }

        


        private void ButtonAjouter_click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("Pages/Enquete/AjouterEnquete.xaml", UriKind.Relative));
        }
    }
}
