﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ecostat
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            using (var db = new ecostatEntities())
            {
                //Si il trouve un user et un mot de passe qui correspondent
                if (db.users.Any(x => x.login == TextBoxLogin.Text && x.mdp == PasswordBox.Password))
                {
                    FenetrePage FenetrePage = new FenetrePage();
                    FenetrePage.Show();

                    this.Close();
                }
                else if (db.users.Any(x => x.login == TextBoxLogin.Text))
                {
                    LabelErreur.Text = "Mauvais mot de passe";
                    PasswordBox.Focus();
                }
                else
                {
                    LabelErreur.Text = "Cet identifiant n'existe pas";
                    TextBoxLogin.Focus();
                }
            }
        }
    }
}
